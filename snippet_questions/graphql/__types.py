import graphene
from graphene_django import DjangoObjectType

# Models
from ..models import QuestionOpen, QuestionYesOrNo, QuestionRange, QuestionMultiple, QuestionSingle, QuestionExplainer


class BaseQuestionType(graphene.Interface):
  id = graphene.Int()
  question_text = graphene.String()
  question_type = graphene.String()
  question_imagelink = graphene.String()


class QuestionOpenType(DjangoObjectType):
  class Meta:
    model = QuestionOpen
    interfaces = [BaseQuestionType]
    fields = ("ngrams", "filter_user_ids_length")


class QuestionYesOrNoType(DjangoObjectType):
  class Meta:
    model = QuestionYesOrNo
    interfaces = [BaseQuestionType]
    fields = ("id",)


class QuestionMultipleType(DjangoObjectType):
  options = graphene.String()

  class Meta:
    model = QuestionMultiple
    interfaces = [BaseQuestionType]
    fields = ("options",)


class QuestionSingleType(DjangoObjectType):
  options = graphene.String()

  class Meta:
    model = QuestionSingle
    interfaces = [BaseQuestionType]
    fields = ("options",)


class QuestionRangeType(DjangoObjectType):
  scale = graphene.String()
  scale_type = graphene.String()

  class Meta:
    model = QuestionRange
    interfaces = [BaseQuestionType]
    fields = ("scale", "scale_type")

class QuestionExplainerType(DjangoObjectType):
  explainer_text = graphene.String()

  class Meta:
    model = QuestionExplainer
    interfaces = [BaseQuestionType]
    fields = ("explainer_text",)
