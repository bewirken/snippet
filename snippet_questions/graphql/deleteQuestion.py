
import graphene
from graphql_jwt.decorators import login_required

# Models
from ..models import Question


class DeleteQuestionMutation(graphene.Mutation):
  success = graphene.Boolean()
  pass

  class Arguments:
    question_id = graphene.ID(required=True)

  @login_required
  def mutate(self, info, question_id):
    question = Question.objects.get(id=question_id)
    question.delete()

    # CRAZY bulk update hack for img-urls after media url change
    # all_questions = Question.objects.all()
    # for question in all_questions:
    #   if(question.question_imagelink):
    #     new_url = question.question_imagelink.replace("data.snippet-live.de", "challenge-app-space.fra1.cdn.digitaloceanspaces.com")
    #     question.question_imagelink = new_url
    #     question.save()

    return DeleteQuestionMutation(success=True)


class DeleteQuestion(graphene.ObjectType):
  delete_question = DeleteQuestionMutation.Field()
