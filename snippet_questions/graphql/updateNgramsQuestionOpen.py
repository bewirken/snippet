import graphene
from graphql_jwt.decorators import login_required

import json

# Types

# Models
from ..models import QuestionOpen

class UpdateNgramsQuestionOpenMutation(graphene.Mutation):
  ngrams = graphene.String()

  class Arguments:
    question_id = graphene.ID(required=True)
    ngrams = graphene.String(required=True)

  @login_required
  def mutate(self, info, question_id, ngrams):

    pending_question = QuestionOpen.objects.get(id = question_id)
    pending_question.ngrams = json.loads(ngrams)
    pending_question.save()

    return UpdateNgramsQuestionOpenMutation(
      ngrams = pending_question.ngrams
    )


class UpdateNgramsQuestionOpen(graphene.ObjectType):
  update_ngrams_question_open = UpdateNgramsQuestionOpenMutation.Field()
