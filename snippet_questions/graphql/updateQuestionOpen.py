import graphene
from graphql_jwt.decorators import login_required

import datetime

# Models
from ..models import QuestionOpen


class UpdateQuestionOpenMutation(graphene.Mutation):
  question_type = graphene.String()
  question_text = graphene.String()
  question_imagelink = graphene.String()
  pass

  class Arguments:
    question_id = graphene.ID(required=True)
    question_text = graphene.String(required=True)
    question_imagelink = graphene.String(required=True)

  @login_required
  def mutate(self, info, question_id, question_text, question_imagelink):
    pending_question = QuestionOpen.objects.get(id=question_id)

    pending_question.question_text = question_text
    pending_question.question_imagelink = question_imagelink

    pending_question.save()

    return UpdateQuestionOpenMutation(
        question_type=pending_question.question_type,
        question_text=pending_question.question_text,
        question_imagelink=pending_question.question_imagelink,
    )


class UpdateQuestionOpen(graphene.ObjectType):
  update_question_open = UpdateQuestionOpenMutation.Field()
