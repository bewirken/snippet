from django.apps import apps as django_apps
import datetime

import graphene
from graphql_jwt.decorators import login_required

# Models
from ..models import QuestionRange


class CreateQuestionRangeMutation(graphene.Mutation):
  id = graphene.Int()
  question_type = graphene.String()
  question_text = graphene.String()
  question_imagelink = graphene.String()
  scale = graphene.String()
  scale_type = graphene.String()

  pass

  class Arguments:
    poll_id = graphene.ID(required=True)
    question_text = graphene.String(required=True)
    question_imagelink = graphene.String(required=True)
    scale = graphene.List(graphene.String, default_value=[])
    scale_type = graphene.String(required=True)

  @login_required
  def mutate(self, info, poll_id, question_text, question_imagelink, scale, scale_type):

    Poll = django_apps.get_model('snippet_polls', 'Poll')

    question = QuestionRange(
        poll=Poll.objects.get(id=poll_id),
        question_type="Range",
        question_text=question_text,
        question_imagelink=question_imagelink,
        scale=scale,
        scale_type=QuestionRange.ScaleType[scale_type.upper()],
    )
    question.save()

    return CreateQuestionRangeMutation(
        id=question.id,
        question_type=question.question_type,
        question_text=question.question_text,
        question_imagelink=question.question_imagelink,
        scale=question.scale,
        scale_type=question.scale_type,
    )


class CreateQuestionRange(graphene.ObjectType):
  create_question_range = CreateQuestionRangeMutation.Field()
