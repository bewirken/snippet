import graphene
from graphql_jwt.decorators import login_required

import datetime

# Models
from ..models import QuestionRange


class UpdateQuestionRangeMutation(graphene.Mutation):
  question_type = graphene.String()
  question_text = graphene.String()
  question_imagelink = graphene.String()
  scale = graphene.String()
  scale_type = graphene.String()
  pass

  class Arguments:
    question_id = graphene.ID(required=True)
    question_text = graphene.String(required=True)
    question_imagelink = graphene.String(required=True)
    scale = graphene.List(graphene.String, default_value=[])
    scale_type = graphene.String(required=True)

  @login_required
  def mutate(self, info, question_id, question_text, question_imagelink, scale, scale_type):
    pending_question = QuestionRange.objects.get(id=question_id)

    pending_question.question_text = question_text
    pending_question.question_imagelink = question_imagelink

    pending_question.scale = scale
    pending_question.scale_type = QuestionRange.ScaleType[scale_type.upper()]

    pending_question.save()

    return UpdateQuestionRangeMutation(
        question_type=pending_question.question_type,
        question_text=pending_question.question_text,
        question_imagelink=pending_question.question_imagelink,
        scale=pending_question.scale,
        scale_type=pending_question.scale_type
    )


class UpdateQuestionRange(graphene.ObjectType):
  update_question_range = UpdateQuestionRangeMutation.Field()
