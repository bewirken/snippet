from django.apps import AppConfig

class SnippetQuestionsConfig(AppConfig):
    name = 'snippet_questions'
