# Generated by Django 3.0.5 on 2020-10-02 09:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('snippet_questions', '0009_auto_20200925_1449'),
    ]

    operations = [
        migrations.AlterField(
            model_name='questionrange',
            name='scale_type',
            field=models.CharField(choices=[('interval', 'Schritte'), ('continuous', 'Fließend (1-10)')], default='interval', max_length=10, verbose_name='Skalentyp'),
        ),
    ]
