import graphene

# Models
from ..models import Tan


class CheckTanMutation(graphene.Mutation):
  success = graphene.Boolean()

  class Arguments:
    poll_id = graphene.ID(required=True)
    tan = graphene.Int(required=True)
    use_tan = graphene.Boolean()
    valid_vote = graphene.Boolean()

  def mutate(self, info, poll_id, tan, **kwargs):
    tan = Tan.objects.get(poll=poll_id, tan=tan)

    use_tan = kwargs.get('use_tan', False)
    valid_vote = kwargs.get('valid_vote', None)

    if not tan.used:

      if use_tan: # use TAN
        tan.used = True
        
      if valid_vote is not None: # validate vote
        if valid_vote:
          tan.valid_vote = True
        else: #delte user if vote is note valid to remove votes/userAnswers from analysis
          tan.valid_vote = False
          info.context.user.delete()

      tan.save()

      success = True # TAN validated
    else:
      success = False # TAN already used

    # Temporary tan logic
    # tans = Tan.objects.all()
    # for tan in tans:
    #   tan.valid_vote = False

    # Tan.objects.bulk_update(tans, fields=['valid_vote'], batch_size=100)

    return CheckTanMutation(success=success)


class CheckTan(graphene.ObjectType):
  check_tan = CheckTanMutation.Field()
