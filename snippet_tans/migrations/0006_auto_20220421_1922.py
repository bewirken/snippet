# Generated by Django 3.0.5 on 2022-04-21 17:22

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('snippet_tans', '0005_auto_20220324_1239'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tanlist',
            name='tan_amount',
            field=models.IntegerField(default=0, validators=[django.core.validators.MaxValueValidator(50000)]),
        ),
    ]
