import graphene
from graphql_jwt.decorators import login_required

import datetime

# Types
from snippet.users.graphql.__types import UserType
from snippet_questions.graphql.__types import QuestionExplainerType


# Models
from ..models import UserAnswerExplainer


class UpdateUserAnswerExplainerMutation(graphene.Mutation):
  question = graphene.Field(QuestionExplainerType)
  first_touched = graphene.types.datetime.DateTime()
  last_touched = graphene.types.datetime.DateTime()
  count_touched = graphene.Int()

  status = graphene.Boolean()
  pass

  class Arguments:
    question_id = graphene.ID(required=True)

  @login_required
  def mutate(self, info, question_id):
    current_user = info.context.user
    pending_answer = UserAnswerExplainer.objects.get(
        user=current_user, question_id=question_id, poll_id=current_user.current_poll.id)

    pending_answer.status = True

    # If answer wasn't answered before set first_touched and last_touched to datetime to now
    if pending_answer.first_touched == None:
      pending_answer.first_touched = datetime.datetime.now()
      pending_answer.last_touched = datetime.datetime.now()
    else:
      pending_answer.last_touched = datetime.datetime.now()

    pending_answer.count_touched += 1
    pending_answer.save()

    return UpdateUserAnswerExplainerMutation(
        status=pending_answer.status,
        question=pending_answer.question,
        first_touched=pending_answer.first_touched,
        last_touched=pending_answer.last_touched,
        count_touched=pending_answer.count_touched,
    )


class UpdateUserAnswerExplainer(graphene.ObjectType):
  update_user_answer_explainer = UpdateUserAnswerExplainerMutation.Field()
