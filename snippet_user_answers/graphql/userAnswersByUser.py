import graphene
from graphql_jwt.decorators import login_required

from itertools import chain
# from django.db.models import Case, When


# Types
from .__types import BaseUserAnswerType

# Models
from ..models import UserAnswerOpen, UserAnswerYesOrNo, UserAnswerMultiple, UserAnswerSingle, UserAnswerRange, UserAnswerExplainer


class UserAnswersByUser(graphene.ObjectType):
  user_answers_by_user = graphene.List(BaseUserAnswerType)

  @login_required
  def resolve_user_answers_by_user(self, info, **kwargs):

    current_user = info.context.user

    open_user_answers = UserAnswerOpen.objects.filter(user=current_user, poll_id=current_user.current_poll.id)
    yes_no_user_answers = UserAnswerYesOrNo.objects.filter(user=current_user, poll_id=current_user.current_poll.id)
    multi_user_answers = UserAnswerMultiple.objects.filter(user=current_user, poll_id=current_user.current_poll.id)
    single_user_answers = UserAnswerSingle.objects.filter(user=current_user, poll_id=current_user.current_poll.id)
    range_user_answers = UserAnswerRange.objects.filter(user=current_user, poll_id=current_user.current_poll.id)
    explainer_user_answers = UserAnswerExplainer.objects.filter(user=current_user, poll_id=current_user.current_poll.id)

    ordered_question_ids = current_user.current_poll.ordered_question_ids

    user_answers_by_user = list(chain(
        open_user_answers,
        yes_no_user_answers,
        multi_user_answers,
        single_user_answers,
        range_user_answers,
        explainer_user_answers
    ))

    if len(user_answers_by_user) == len(ordered_question_ids):
      user_answers_by_user = sorted(
          user_answers_by_user, key=lambda user_answer: ordered_question_ids.index(user_answer.question.id))
    else:
      print("WARNING: ordered_question_ids does not contain all userAnswer ids")

    return user_answers_by_user
