import graphene

from graphql_jwt.decorators import login_required
from django.apps import apps as django_apps

class UserAnswersByQuestionCounted(graphene.ObjectType):
  user_answers_by_question_counted = graphene.List(graphene.Int, 
    question_id=graphene.ID(required=True), 
    question_type=graphene.String(required=True),
    user_ids = graphene.List(graphene.Int)
  )

  @login_required
  def resolve_user_answers_by_question_counted(self, info, question_id, question_type, **kwargs):

    Question = django_apps.get_model('snippet_questions', 'Question' + question_type)

    filter_params = {"question_id":question_id, "status":True}

    user_ids=kwargs.get('user_ids', None)
    question = Question.objects.get(id=question_id)
    
    if user_ids != None:
      filter_params["user_id__in"] = user_ids
      return count_by_question_type(question, filter_params)

    else:
      return count_by_question_type(question, filter_params)

def count_by_question_type(question, filter_params):

  if question.question_type == "YesOrNo": 
    num_of_answer_fields=2
    answer_selected_field="answer_value"
  elif question.question_type == "Single":
    num_of_answer_fields=len(question.options)
    answer_selected_field="answer_choice_key"
  elif question.question_type == "Multiple":  
    num_of_answer_fields=len(question.options)
    answer_selected_field="answer_choice_keys__contains"
  elif question.question_type == "Range":
    num_of_answer_fields=len(question.scale)
    answer_selected_field="answer_scale_key"

  UserAnswerModel = django_apps.get_model('snippet_user_answers', 'UserAnswer' + question.question_type)


  user_answers_counted = []
  for index in range(num_of_answer_fields):
    user_answers_counted.append(UserAnswerModel.objects.filter(**filter_params, **{answer_selected_field: [index] if question.question_type == "Multiple" else index}).count())
  
  # add negative num of participants to data (always last element and negative)
  # TODO replace with GraphQL type {user_answers_counted, num_of_participants}

  user_answers_counted.append(-abs(UserAnswerModel.objects.filter(**filter_params).count()))

  return user_answers_counted