import graphene
from graphql_jwt.decorators import login_required

import datetime

# Types
from snippet.users.graphql.__types import UserType
from snippet_questions.graphql.__types import QuestionSingleType


# Models
from ..models import UserAnswerSingle


class UpdateUserAnswerSingleMutation(graphene.Mutation):
  question = graphene.Field(QuestionSingleType)
  first_touched = graphene.types.datetime.DateTime()
  last_touched = graphene.types.datetime.DateTime()
  count_touched = graphene.Int()

  status = graphene.Boolean()
  answer_choice_key = graphene.Int()
  pass

  class Arguments:
    question_id = graphene.ID(required=True)
    answer_choice_key = graphene.Int()

  @login_required
  def mutate(self, info, question_id, answer_choice_key):
    current_user = info.context.user
    pending_answer = UserAnswerSingle.objects.get(
        user=current_user, question_id=question_id, poll_id=current_user.current_poll.id)

    # Check if the array of submitted answer_choice_key is not longer then the array of possible keys (options)
    pending_answer.answer_choice_key = answer_choice_key

    # If choice is made mark answer as answered (status=True)
    if answer_choice_key != -1:
      pending_answer.status = True
    else:
      pending_answer.status = False

    # If answer wasn't answered before set first_touched and last_touched to datetime to now
    if pending_answer.first_touched == None:
      pending_answer.first_touched = datetime.datetime.now()
      pending_answer.last_touched = datetime.datetime.now()
    else:
      pending_answer.last_touched = datetime.datetime.now()

    pending_answer.count_touched += 1
    pending_answer.save()

    return UpdateUserAnswerSingleMutation(
        status=pending_answer.status,
        question=pending_answer.question,
        first_touched=pending_answer.first_touched,
        last_touched=pending_answer.last_touched,
        count_touched=pending_answer.count_touched,
        answer_choice_key=pending_answer.answer_choice_key
    )


class UpdateUserAnswerSingle(graphene.ObjectType):
  update_user_answer_single = UpdateUserAnswerSingleMutation.Field()
