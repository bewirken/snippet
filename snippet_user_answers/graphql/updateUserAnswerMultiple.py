import graphene
from graphql_jwt.decorators import login_required

import datetime

# Types
from snippet.users.graphql.__types import UserType
from snippet_questions.graphql.__types import QuestionMultipleType


# Models
from ..models import UserAnswerMultiple


# class answerChoiceKeysInput(graphene.InputObjectType):
#   keys = graphene.List(graphene.Int)


class UpdateUserAnswerMultipleMutation(graphene.Mutation):
  question = graphene.Field(QuestionMultipleType)
  first_touched = graphene.types.datetime.DateTime()
  last_touched = graphene.types.datetime.DateTime()
  count_touched = graphene.Int()
  status = graphene.Boolean()
  answer_choice_keys = graphene.List(graphene.Int)
  pass

  class Arguments:
    question_id = graphene.ID(required=True)
    answer_choice_keys = graphene.List(graphene.Int)

  @login_required
  def mutate(self, info, question_id, answer_choice_keys):
    current_user = info.context.user
    pending_answer = UserAnswerMultiple.objects.get(
        user=current_user, question_id=question_id, poll_id=current_user.current_poll.id)

    # Check if the array of submitted answer_choice_keys is not longer then the array of possible keys (options)
    if len(answer_choice_keys) > len(pending_answer.question.options):
      raise Exception('Choice not available')
    else:
      pending_answer.answer_choice_keys = answer_choice_keys

    # If choices are made mark answer as answered (status=True)
    if len(answer_choice_keys) > 0:
      pending_answer.status = True
    else:
      pending_answer.status = False

    # If answer wasn't answered before set first_touched and last_touched to datetime to now
    if pending_answer.first_touched == None:
      pending_answer.first_touched = datetime.datetime.now()
      pending_answer.last_touched = datetime.datetime.now()
    else:
      pending_answer.last_touched = datetime.datetime.now()

    pending_answer.count_touched += 1
    pending_answer.save()

    return UpdateUserAnswerMultipleMutation(
        status=pending_answer.status,
        question=pending_answer.question,
        first_touched=pending_answer.first_touched,
        last_touched=pending_answer.last_touched,
        count_touched=pending_answer.count_touched,
        answer_choice_keys=pending_answer.answer_choice_keys
    )


class UpdateUserAnswerMultiple(graphene.ObjectType):
  update_user_answer_multiple = UpdateUserAnswerMultipleMutation.Field()
