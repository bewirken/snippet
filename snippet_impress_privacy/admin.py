from django.contrib import admin

# Register your models here.
from .models import Impress, PrivacyPolicy, PrivacyTopic

class ImpressAdmin(admin.ModelAdmin):
  pass

class PrivacyTopicInline(admin.StackedInline):
  model =  PrivacyTopic
  extra = 0

class PrivacyPolicyAdmin(admin.ModelAdmin):
  inlines = [PrivacyTopicInline]


admin.site.register(Impress, ImpressAdmin)
admin.site.register(PrivacyPolicy, PrivacyPolicyAdmin)
