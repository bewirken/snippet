from django.db import models
from django.conf import settings

from ckeditor.fields import RichTextField


class Impress(models.Model):
    account = models.OneToOneField("snippet_accounts.Account", on_delete=models.CASCADE)
    content = RichTextField(("Inhalt"), default="", config_name='extended')

    def __str__(self):
      return self.account.user.username

class PrivacyPolicy(models.Model):
    account = models.OneToOneField("snippet_accounts.Account", on_delete=models.CASCADE)
    intro = RichTextField(("Einleitung"), default="", config_name='extended')

    def __str__(self):
      return self.account.user.username

class PrivacyTopic(models.Model):
    privacy_policy = models.ForeignKey(PrivacyPolicy, on_delete=models.CASCADE)
    title = models.CharField(("Thema"), max_length=250, blank=True, null=True)
    content = RichTextField(("Inhalt"), default="", config_name='extended')

    def __str__(self):
      return self.title



