from django.conf import settings
import requests
import json

import graphene
from graphql_jwt.decorators import login_required


# Models
from ..models import Image


class PurgeImageCacheMutation(graphene.Mutation):
  success = graphene.Boolean()
  pass

  class Arguments:
    src = graphene.String(required=True)

  @login_required
  def mutate(self, info, src):

    payload = {"files": ["media/images/" + src]}
    headers = {
        "content-type": "application/json",
        "Authorization": "Bearer " + settings.DIGITAL_OCEAN_API_TOKEN
    }
    url = "https://api.digitalocean.com/v2/cdn/endpoints/" + settings.AWS_S3_ENDPOINT_ID + "/cache"

    requests.delete(
        url,
        data=json.dumps(payload),
        headers=headers,
    )

    return PurgeImageCacheMutation(success=True)


class PurgeImageCache(graphene.ObjectType):
  purge_image_cache = PurgeImageCacheMutation.Field()
