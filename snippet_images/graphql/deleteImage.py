import graphene
from graphql_jwt.decorators import login_required


# Models
from ..models import Image


class DeleteImageMutation(graphene.Mutation):
  src = graphene.String()
  pass

  class Arguments:
    uuid = graphene.String(required=True)

  @login_required
  def mutate(self, info, uuid):
    image = Image.objects.get(uuid=uuid)
    src = image.owner.username + "/" + image.title.lower()
    
    image.delete()

    return DeleteImageMutation(src=src)


class DeleteImage(graphene.ObjectType):
  delete_image = DeleteImageMutation.Field()
