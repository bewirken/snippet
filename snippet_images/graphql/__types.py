from graphene_django import DjangoObjectType

# Models
from ..models import Image


class ImageType(DjangoObjectType):
  class Meta:
    model = Image
    fields = ('uuid','src', 'width', 'height',)
