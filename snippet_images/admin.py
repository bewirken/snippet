from django.contrib import admin

from .models import Image


class ImageAdmin(admin.ModelAdmin):
  model = Image
  list_display = ('title', 'owner', 'uuid', 'src', 'width', 'height')

  # def has_add_permission(self, request):
  #   return False
  # def has_delete_permission(self, request, obj=None):
  #   return False

# Register your models here.
admin.site.register(Image, ImageAdmin)
