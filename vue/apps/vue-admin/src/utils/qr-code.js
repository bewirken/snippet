export default function qrCodeToPNG(pollCode) {
  let canvas = document.querySelector(`.qrcode-${pollCode}`).firstChild

  canvas.toBlob(function (blob) {
    let url = window.URL.createObjectURL(blob)

    let a = document.createElement("a")
    a.href = url
    a.download = `.QR-Code-${pollCode}`
    document.body.appendChild(a) // Required for FF

    a.click()
    a.remove()
    window.URL.revokeObjectURL(url)
  })
}
