import apolloProvider from "@/apollo.config"

import { ANALYTICS_USER_ANSWERS_BY_QUESTION } from "vue-shared"

export const getAnalyticsUserAnswersByQuestion = (data) => {
  let variables = {
    questionId: data.question.id,
    questionType: data.question.questionType,
  }

  // Check if conditionals are set
  if (data.filterQuestionUserIds && data.filterQuestionUserIds.length) {
    variables.userIds = data.filterQuestionUserIds
  }

  return apolloProvider.defaultClient.query({
    query: ANALYTICS_USER_ANSWERS_BY_QUESTION,
    fetchPolicy: "network-only",
    variables: variables,
  })
}
