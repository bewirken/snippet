import { defineStore } from "pinia"

export const useWordcloudStore = defineStore({
  id: "wordcloud",
  state: () => ({
    ngrams: [],
    selectedNgrams: [],

    showWordcloudEditor: false,
    wordcloudPDFLoading: false,

    searchTerm: "",
  }),
  getters: {},
  actions: {},
})
