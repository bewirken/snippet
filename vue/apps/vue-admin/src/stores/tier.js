import { defineStore } from "pinia"

import { useUserStore } from "@/stores/user"

import apolloProvider from "@/apollo.config"
import { CURRENT_ACCOUNT } from "vue-shared"

export const useTierStore = defineStore({
  id: "tier",
  state: () => ({
    tier: {
      accountActive: false,
      wooSubscriptionId: null,
      id: null,
      name: '"Ups - Account inaktiv"',
      pollsAmount: 0,
      questionsAmount: 0,
      participantsAmount: 0,
      userAnswersAmount: 0,
      imageUploadsAmount: 0,
      conditionalAnalysis: false,
      wordcloudEditor: false,
      disableUpgrade: false,
      emailSupport: false,
      endActions: [],
      questionTypes: [],
      __typename: "",
    },
  }),
  getters: {
    wooAccountURL(state) {
      if (state.tier.wooSubscriptionId) {
        return `https://askit-app.de/adminbereich/abonnement-ansehen/${state.tier.wooSubscriptionId}`
      } else {
        return `https://askit-app.de/adminbereich/abonnements`
      }
    },
  },
  actions: {
    async getTier() {
      return apolloProvider.defaultClient
        .query({
          query: CURRENT_ACCOUNT,
          fetchPolicy: "network-only",
        })
        .then((data) => {
          if (data.data.currentAccount.active) {
            const account = data.data.currentAccount
            useUserStore().username = account.user.username

            const rawTier = account.tier

            let tier = {
              accountActive: data.data.currentAccount.active,
              wooSubscriptionId: data.data.currentAccount.wooSubscriptionId,
              id: rawTier.id,
              name: rawTier.name,
              pollsAmount: rawTier.pollsAmount,
              questionsAmount: rawTier.questionsAmount,
              participantsAmount: rawTier.participantsAmount,
              userAnswersAmount: rawTier.userAnswersAmount,
              imageUploadsAmount: rawTier.imageUploadsAmount,
              conditionalAnalysis: rawTier.conditionalAnalysis,
              wordcloudEditor: rawTier.wordcloudEditor,
              disableUpgrade: rawTier.disableUpgrade,
              emailSupport: rawTier.emailSupport,
              endActions: [],
              questionTypes: [],
            }

            rawTier.endActions.forEach((action) => {
              tier.endActions.push(action.name)
            })
            rawTier.questionTypes.forEach((type) => {
              tier.questionTypes.push(type.name)
            })
            this.tier = tier
          }
          return this.tier
        })
    },
  },
})
