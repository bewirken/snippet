import { defineStore } from "pinia"

import apolloProvider from "@/apollo.config"
import { ALL_HINTS } from "vue-shared"

export const useHintStore = defineStore({
  id: "hint",
  state: () => ({
    hints: {},
  }),
  getters: {},
  actions: {
    async getHints() {
      return apolloProvider.defaultClient
        .query({
          query: ALL_HINTS,
          fetchPolicy: "network-only",
        })
        .then((data) => {
          if (data.data.allHints) {
            let hints = {}

            data.data.allHints.forEach((hint) => {
              hints[hint.slug] = hint.text
            })

            this.hints = hints
          }
          return this.hints
        })
    },
  },
})
