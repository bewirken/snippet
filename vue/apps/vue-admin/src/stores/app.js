import { defineStore } from "pinia"

export const useAppStore = defineStore({
  id: "app",
  state: () => ({
    sidebar: {
      opened: true,
      withoutAnimation: true,
    },
    mediaManager: {
      opened: false,
      context: null,
      card: null,
    },
    limitDialog: {
      opened: false,
      limitType: null,
    },
    device: "desktop",
  }),
  getters: {},
  actions: {
    toggleSideBar() {
      this.sidebar.opened = !this.sidebar.opened
    },
    closeSideBar() {
      this.sidebar.opened = false
    },
    toggleDevice(device) {
      this.device = device
    },
    toggleMediaManager(data) {
      this.mediaManager.opened = !this.mediaManager.opened
      this.mediaManager.context = data.context
      this.mediaManager.card = data.card
    },
    toggleLimitDialog(data) {
      this.limitDialog.opened = !this.limitDialog.opened
      this.limitDialog.limitType = data.limitType
    },
  },
})
