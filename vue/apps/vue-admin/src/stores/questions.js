import { defineStore } from "pinia"
import Sortable from "sortablejs"
import { Notification, MessageBox } from "element-ui"

import {
  convertJsonToObjectArray,
  convertObjectArrayToNormalArray,
  generateDefaultContinuousScale,
} from "@/utils/question-helpers"

import apolloProvider from "@/apollo.config"

import { QUESTIONS_BY_POLL } from "vue-shared"
import { UPDATE_POLL_QUESTION_ORDER } from "vue-shared"

import { CREATE_QUESTION_SINGLE } from "vue-shared"
import { CREATE_QUESTION_MULTIPLE } from "vue-shared"
import { CREATE_QUESTION_RANGE } from "vue-shared"
import { CREATE_QUESTION_YES_OR_NO } from "vue-shared"
import { CREATE_QUESTION_OPEN } from "vue-shared"

import { UPDATE_QUESTION_SINGLE } from "vue-shared"
import { UPDATE_QUESTION_MULTIPLE } from "vue-shared"
import { UPDATE_QUESTION_RANGE } from "vue-shared"
import { UPDATE_QUESTION_YES_OR_NO } from "vue-shared"
import { UPDATE_QUESTION_OPEN } from "vue-shared"

import { DELETE_QUESTION } from "vue-shared"

import { usePollStore } from "@/stores/polls"
import { useTierStore } from "@/stores/tier"

export const useQuestionStore = defineStore({
  id: "question",
  state: () => ({
    questions: [],
    pendingQuestion: {
      id: null,
      questionText: null,
      questionType: null,
      questionImagelink: null,
      options: null,
      optionsForm: null,
      scale: null,
      scaleForm: null,
      scaleType: null,
      previousScale: null,
    },
    dialogQuestionFormVisible: false,
    dialogQuestionTypeChoiceVisible: false,
    dialogState: "",
    showMissingQuestionTextMsg: false,
    showMissingOptionTextMsg: false,
    showMissingOptionsMsg: false,
  }),
  getters: {
    questionTypeMap() {
      let questionTypes = {
        YesOrNo: "Ja/Nein",
        Single: "Einfachauswahl",
        Multiple: "Mehrfachauswahl",
        Range: "Skala",
        Open: "Freitext",
      }
      for (const [key, _] of Object.entries(questionTypes)) {
        if (!useTierStore().tier.questionTypes.includes(key)) {
          delete questionTypes[`${key}`]
        }
      }
      return questionTypes
    },
    wrappedPendingQuestion() {
      return {
        question: this.pendingQuestion,
        __typename: `UserAnswer${this.pendingQuestion.questionType}Type`,
      }
    },
  },
  actions: {
    fetchQuestionsByPoll(pollId) {
      apolloProvider.defaultClient
        .query({
          query: QUESTIONS_BY_POLL,
          fetchPolicy: "network-only",
          variables: {
            pollId: pollId,
          },
        })
        .then((response) => (this.questions = response.data.questionsByPoll))
    },
    showQuestionTypeDialog() {
      if (this.questions.length < useTierStore().tier.questionsAmount) {
        this.dialogQuestionTypeChoiceVisible = true
      } else {
        this.appStore.toggleLimitDialog({ limitType: "question" })
      }
    },
    handleEditQuestion(question) {
      this.dialogState = "UPDATE"
      this.pendingQuestion = {
        id: question.id,
        questionText: question.questionText,
        questionType: question.questionType,
        questionImagelink: question.questionImagelink,

        optionsForm: question.options ? convertJsonToObjectArray(question.options) : null,
        options: question.options,

        scaleForm: question.scale ? convertJsonToObjectArray(question.scale) : null,
        scale: question.scale,
        scaleType: question.scaleType,
        previousScale: [],
      }
      //  END Check if scale type is number or string
      this.dialogQuestionFormVisible = true
    },
    handleCreateQuestion(questionType) {
      this.dialogState = "CREATE"
      this.pendingQuestion = {
        questionText: "",
        questionType: questionType,
        questionImagelink: "",

        optionsForm: questionType == "Multiple" || questionType == "Single" ? [] : null,
        options: "",

        scaleType: questionType == "Range" ? "continuous" : null,
        scaleForm: questionType == "Range" ? generateDefaultContinuousScale() : null,
        scale: "",
      }
      this.dialogQuestionFormVisible = true
      // Delay to prevent dialog overlay flickering
      setTimeout(() => (this.dialogQuestionTypeChoiceVisible = false), 250)
    },
    saveQuestion() {
      // Executes either a createQuestion or updateQuestion mutation depending on the questionStore.dialogState
      // Define constant mutations params
      let mutationParams = {
        mutation: null,
        variables: {
          questionText: this.pendingQuestion.questionText,
          questionImagelink: this.pendingQuestion.questionImagelink,
        },
      }
      // Check dialog state to add needed id for updateQuestion-mutation (createQuestion-mutation requires no id)
      if (this.dialogState == "UPDATE") {
        mutationParams.variables.questionId = this.pendingQuestion.id
      } else if (this.dialogState == "CREATE") {
        mutationParams.variables.pollId = usePollStore().activePoll.id
      }
      // Add mutation params depending on question type
      switch (this.pendingQuestion.questionType) {
        case "Open":
          if (!this.validateInput(this.pendingQuestion.questionText, null)) {
            return
          }
          mutationParams.mutation =
            this.dialogState == "UPDATE" ? UPDATE_QUESTION_OPEN : CREATE_QUESTION_OPEN
          break
        case "YesOrNo":
          if (!this.validateInput(this.pendingQuestion.questionText, null)) {
            return
          }
          mutationParams.mutation =
            this.dialogState == "UPDATE" ? UPDATE_QUESTION_YES_OR_NO : CREATE_QUESTION_YES_OR_NO
          break
        case "Multiple":
          if (
            !this.validateInput(this.pendingQuestion.questionText, this.pendingQuestion.optionsForm)
          ) {
            return
          }
          mutationParams.mutation =
            this.dialogState == "UPDATE" ? UPDATE_QUESTION_MULTIPLE : CREATE_QUESTION_MULTIPLE
          mutationParams.variables.options = convertObjectArrayToNormalArray(
            this.pendingQuestion.optionsForm
          )
          break
        case "Single":
          if (
            !this.validateInput(this.pendingQuestion.questionText, this.pendingQuestion.optionsForm)
          ) {
            return
          }
          mutationParams.mutation =
            this.dialogState == "UPDATE" ? UPDATE_QUESTION_SINGLE : CREATE_QUESTION_SINGLE
          mutationParams.variables.options = convertObjectArrayToNormalArray(
            this.pendingQuestion.optionsForm
          )
          break
        case "Range":
          if (
            !this.validateInput(this.pendingQuestion.questionText, this.pendingQuestion.scaleForm)
          ) {
            return
          }
          mutationParams.mutation =
            this.dialogState == "UPDATE" ? UPDATE_QUESTION_RANGE : CREATE_QUESTION_RANGE
          mutationParams.variables.scale = convertObjectArrayToNormalArray(
            this.pendingQuestion.scaleForm
          )
          // Because of graphene-type reasons the "scaleType" mutation param needs to be upperCase
          mutationParams.variables.scaleType = this.pendingQuestion.scaleType
          break
      }
      apolloProvider.defaultClient
        .mutate(mutationParams)
        .then((data) => {
          // Update UI after succesful mutations (updateQuestion/createQuestion)
          if (this.dialogState == "UPDATE") {
            let currentQuestionList = this.questions.find((x) => x.id === this.pendingQuestion.id)

            let updatedQuestion = data.data["updateQuestion" + this.pendingQuestion.questionType]

            currentQuestionList.questionText = updatedQuestion.questionText
            currentQuestionList.questionImagelink = updatedQuestion.questionImagelink
            currentQuestionList.options = updatedQuestion.options
            currentQuestionList.scale = updatedQuestion.scale
            currentQuestionList.scaleType = updatedQuestion.scaleType
          } else if (this.dialogState == "CREATE") {
            this.questions.push(data.data["createQuestion" + this.pendingQuestion.questionType])
          }
          // Notify and close dialog
          Notification({
            message: "Frage gespeichert",
            showClose: true,
            duration: 1500,
            position: "bottom-left",
            type: "success",
          })
          this.dialogQuestionFormVisible = false
        })
        .catch((error) => {
          // Error
          console.error(error)
        })
    },
    deleteQuestion(questionId) {
      MessageBox.confirm(
        "Wenn du diese Frage löscht, werden auch alle zugehörigen ANTWORTEN gelöscht! Frage wirklich unwiderruflich löschen?",
        "Frage löschen",
        {
          confirmButtonText: "Ja",
          cancelButtonText: "Abbrechen",
          confirmButtonClass: "confirm-delete-button",
          // iconClass: "el-icon-delete",
          type: "error",
        }
      )
        .then(() => {
          apolloProvider.defaultClient
            .mutate({
              mutation: DELETE_QUESTION,
              variables: {
                questionId: questionId,
              },
            })
            .then(() => {
              // Remove deleted question from questionStore.ques
              let question = this.questions.find((x) => x.id === questionId)
              let index = this.questions.indexOf(question)
              this.questions.splice(index, 1)

              // Update UI after succesful mutations (updateQuestion/createQuestion)
              // Notify and close dialog
              Notification({
                message: "Frage gelöscht",
                showClose: true,
                duration: 1500,
                position: "bottom-left",
                type: "success",
              })
              this.dialogQuestionFormVisible = false
            })
            .catch((error) => {
              // Error
              console.error(error)
            })
        })
        .catch(() => {
          this.dialogQuestionFormVisible = false
        })
    },
    enableQuestionSorting(element) {
      this.sortable = Sortable.create(element, {
        ghostClass: "sortable-ghost", // Class name for the drop placeholder,
        setData: function (dataTransfer) {
          // to avoid Firefox bug
          // Detail see : https://github.com/RubaXa/Sortable/issues/1012
          dataTransfer.setData("Text", "")
        },
        onEnd: (evt) => {
          const targetRow = this.questions.splice(evt.oldIndex, 1)[0]
          this.questions.splice(evt.newIndex, 0, targetRow)

          let newOrderedQuestionIds = this.questions.map((v) => v.id)

          apolloProvider.defaultClient
            .mutate({
              mutation: UPDATE_POLL_QUESTION_ORDER,
              variables: {
                pollId: usePollStore().activePoll.id,
                orderedQuestionIds: newOrderedQuestionIds,
              },
            })
            .catch((error) => {
              console.error(error)
            })
        },
      })
    },
    validateInput(questionText, options) {
      let validated = true

      if (!questionText) {
        this.showMissingQuestionTextMsg = true
        validated = false
      }
      if (options) {
        if (options.length < 2) {
          this.showMissingOptionsMsg = true
          validated = false
        }
        for (const option of options) {
          if (option.value == "") {
            this.showMissingOptionTextMsg = true
            validated = false
          }
        }
      }
      return validated
    },
  },
})
