import router from "./router"
import NProgress from "nprogress" // progress bar
import "@/styles/nprogress.css" // progress bar style
import { getToken, removeToken } from "@/utils/auth" // get token from cookie

import apolloProvider from "@/apollo.config"

import { VERIFY_TOKEN } from "vue-shared"

NProgress.configure({ showSpinner: false }) // NProgress Configuration

const whiteList = ["/login"] // no redirect whitelist

router.beforeEach(async (to, from, next) => {
  // start progress bar
  NProgress.start()

  // set page title
  document.title = getPageTitle(to.meta.title)

  // determine whether the user has logged in
  const token = getToken()

  if (token) {
    if (to.path === "/login") {
      // if is logged in, redirect to the home page
      next({ path: "/" })
      NProgress.done()
    } else {
      apolloProvider.defaultClient
        .mutate({
          mutation: VERIFY_TOKEN,
          variables: {
            token: token,
          },
        })
        .then(() => {
          NProgress.done()
        })
        .catch(() => {
          next(`/login?redirect=${to.path}`)
          removeToken()
          NProgress.done()
        })
      next()
    }
  } else {
    /* has no token*/
    if (whiteList.indexOf(to.path) !== -1) {
      // in the free login whitelist, go directly
      next()
    } else {
      // other pages that do not have permission to access are redirected to the login page.
      next(`/login?redirect=${to.path}`)
      NProgress.done()
    }
  }
})

router.afterEach(() => {
  // finish progress bar
  NProgress.done()
})

function getPageTitle(pageTitle) {
  if (pageTitle) {
    return `${pageTitle} - askit Adminbereich`
  }
  return "askit Adminbereich"
}
