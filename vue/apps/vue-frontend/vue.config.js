const path = require("path")
const BundleTracker = require("webpack-bundle-tracker")
// const CompressionPlugin = require('compression-webpack-plugin');

function resolve(dir) {
  return path.join(__dirname, dir)
}
// Placeholder plugin to allow ternariy "if or DO NOTHING" webpack config
function NullPlugin() {}
NullPlugin.prototype.apply = function () {}

module.exports = {
  outputDir: "../../../snippet/static/vue-frontend",
  publicPath:
    process.env.NODE_ENV === "production"
      ? `https://challenge-app-space.fra1.cdn.digitaloceanspaces.com/${process.env.VUE_APP_SRC_FOLDER}/vue-frontend/`
      : "/",
  productionSourceMap: false,
  devServer: {
    host: "5.75.249.107",
    port: "8080",
    open: false,
    overlay: {
      warnings: false,
      errors: true,
    },
  },
  configureWebpack: {
    plugins: [
      process.env.NODE_ENV === "production"
        ? new BundleTracker({
            path: ".",
            filename: "../../../snippet/static/vue-frontend/webpack-stats-frontend.json",
            // filename: `https://challenge-app-space.fra1.cdn.digitaloceanspaces.com/${process.env.VUE_APP_SRC_FOLDER}/vue-frontend/webpack-stats-frontend.json`,
          })
        : new NullPlugin(),
      // process.env.NODE_ENV === 'production'
      //   ? new CompressionPlugin({
      //     filename: '[path].gz[query]',
      //     algorithm: 'gzip',
      //     test: /\.js$|\.css$|\.html$|\.eot?.+$|\.ttf?.+$|\.woff?.+$|\.svg?.+$/,
      //     threshold: 10240,
      //     minRatio: 0.8
      //   })
      //   : new NullPlugin()
    ],
    name: "vue-frontend",
    resolve: {
      alias: {
        "@": resolve("src"),
      },
    },
  },
  chainWebpack: (config) => {
    // Fix Safari Hot-Reload bug
    if (process.env.NODE_ENV === "development") {
      config.output.filename("[name].[hash].js").end()
    }
    // GraphQL Loader
    config.module
      .rule("graphql")
      .test(/\.(gql|graphql)$/)
      .use("graphql-tag/loader")
      .loader("graphql-tag/loader")
      .end()
    // Fonts loader
    config.module
      .rule("fonts")
      .test(/\.(woff|woff2|eot|ttf|otf)$/)
      .use("file-loader")
      .loader("file-loader")
      .end()
  },
  // css: {
  //   loaderOptions: {
  //     sass: {
  //       // implementation: require('sass'),
  //       // prependData: `@import "@/variables.scss";`
  //     }
  //   }
  // }
}
