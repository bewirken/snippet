// import fetch from 'unfetch'

import Vue, { markRaw } from "vue"
import VeeValidate from "vee-validate"
import VueI18n from "vue-i18n"
// import VueGtag from "vue-gtag"

import App from "./App.vue"

import { createPinia, PiniaVuePlugin } from "pinia"

import router from "./router"
import apolloProvider from "./apollo.config"

import { messages } from "vue-shared"

import "./components/global"

Vue.config.productionTip = false

Vue.use(PiniaVuePlugin)
const pinia = createPinia()
pinia.use(({ store }) => {
  store.router = markRaw(router)
})
// Install the vue plugin
Vue.use(VueI18n)
Vue.use(VeeValidate, { fieldsBagName: "veeFields" })

// Vue.use(VueGtag, {
//   config: { id: "G-XF9HQJR98F" }
// }, router);

const i18n = new VueI18n({
  locale: "de",
  fallbackLocale: "de",
  messages,
})

new Vue({
  router,
  i18n,
  pinia,
  apolloProvider,
  render: (h) => h(App),
}).$mount("#app")
