from django.apps import AppConfig


class SnippetNotificationsConfig(AppConfig):
    name = 'snippet_notifications'
