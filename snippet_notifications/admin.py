from django.contrib import admin


from .models import NotificationEmail, NotificationHint

class NotificationEmailAdmin(admin.ModelAdmin):
  model = NotificationEmail

class NotificationHintAdmin(admin.ModelAdmin):
  model = NotificationHint

admin.site.register(NotificationHint, NotificationHintAdmin)
admin.site.register(NotificationEmail, NotificationEmailAdmin)
