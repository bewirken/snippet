from graphene_django import DjangoObjectType

# Models
from ..models import Account


class AccountType(DjangoObjectType):
  class Meta:
    model = Account
    fields = ('id',
              'active',
              'user',
              'email',
              'tier',
              'expiration_date',
              'renewal_date',
              'woo_subscription_id',
              'created_at', )
