import graphene
from django.contrib.auth import  get_user_model
from django.conf import settings

# Types
from .__types import AccountType

# Models
from ..models import Account
from snippet_tiers.models import Tier
from snippet_polls.models import Poll

User = get_user_model()

class UpdateAccountMutation(graphene.Mutation):
  account = graphene.Field(AccountType)

  class Arguments:
    username = graphene.String(required=True)
    active = graphene.Int(required=True)
    tier_id = graphene.Int()
    renewal_date = graphene.DateTime()
    expiration_date = graphene.DateTime()

  # TODO @specific_IP_required
  def mutate(self, info, username, **kwargs):
    if info.context.META.get("HTTP_ORIGIN") not in settings.CORS_ORIGIN_WHITELIST:
      raise Exception('You are not authorized to do this!')

    current_user = User.objects.get(unique_identifier=username)
    current_account = Account.objects.select_related("tier").get(user=current_user)

    active = kwargs.get('active', None)
    tier_id = kwargs.get('tier_id', None)
    renewal_date = kwargs.get('renewal_date', None)
    expiration_date = kwargs.get('expiration_date', None)

    old_polls_amount = current_account.tier.polls_amount
    if tier_id:
      tier = Tier.objects.get(id=tier_id)
      new_polls_amount = tier.polls_amount

      if new_polls_amount < old_polls_amount:
        active_polls = Poll.objects.filter(poll_owner = current_user, poll_status = Poll.Status.ACTIVE).count()
        if new_polls_amount < active_polls:
          managed_polls = Poll.objects.filter(poll_owner = current_user, poll_status = Poll.Status.ACTIVE).order_by('-updated_at')[new_polls_amount:]

          for poll in managed_polls:
            poll.poll_status = Poll.Status.INACTIVE
            poll.save()

    current_account.active = (active == 1)
    if renewal_date: current_account.renewal_date = renewal_date
    if expiration_date: current_account.expiration_date = expiration_date
    if tier_id: current_account.tier_id = tier_id


    current_account.save()

    return UpdateAccountMutation(account=current_account)


class UpdateAccount(graphene.ObjectType):
  update_account = UpdateAccountMutation.Field()


