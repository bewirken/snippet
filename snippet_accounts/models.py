from django.db import models
from django.conf import settings
from django.contrib.auth.models import Group

from snippet_polls.models import Poll


class Account(models.Model):
  active = models.BooleanField(default=False)
  email = models.EmailField(default='')
  user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, null=True, blank=True)
  tier = models.ForeignKey('snippet_tiers.Tier', on_delete=models.PROTECT, null=True, blank=True)
  expiration_date = models.DateTimeField(null=True, blank=True)
  renewal_date = models.DateTimeField(null=True, blank=True)
  woo_subscription_id = models.IntegerField(null=True, blank=True)
  created_at = models.DateTimeField(auto_now_add=True)
  updated_at = models.DateTimeField(auto_now=True)

  def save(self, *args, **kwargs):

    self.user.email = self.email
    self.user.save()

    manager = Group.objects.get(name='Manager')
    self.user.groups.add(manager)

    super(Account, self).save(*args, **kwargs)
    if not self.active:
       disablePollsInAccount(self.user)


  def __str__(self):
    return self.user.username

def disablePollsInAccount(user):
    polls = Poll.objects.filter(poll_owner = user)
    for poll in polls:
      poll.poll_status = Poll.Status["INACTIVE"]
      poll.save()