from django.contrib import admin

# Models
from .models import Account


class AccountAdmin(admin.ModelAdmin):
  model = Account
  list_display = [
      'user',
      'active',
      'tier',
      'email',
      'expiration_date',
      'renewal_date',
      'created_at',
  ]

  readonly_fields = [
      'woo_subscription_id',
  ]


admin.site.register(Account, AccountAdmin)
