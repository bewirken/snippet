from django.db import models

from django.contrib.postgres.fields import ArrayField


class TierQuestionType(models.Model):
  name = models.CharField(max_length=154, unique=True)

  def __str__(self):
    return str(self.name)


class TierEndAction(models.Model):
  name = models.CharField(max_length=154, unique=True)

  def __str__(self):
    return str(self.name)


class Tier(models.Model):
  # override_tier = models.ForeignKey('snippet_polls.Tier', default=1, on_delete=models.PROTECT)

  name = models.CharField(max_length=48)

  polls_amount = models.PositiveIntegerField(default=0)
  questions_amount = models.PositiveIntegerField(default=0)
  participants_amount = models.PositiveIntegerField(default=0)
  user_answers_amount = models.PositiveIntegerField(default=0)
  image_uploads_amount = models.PositiveIntegerField(default=0)
  conditional_analysis = models.BooleanField(default=False)
  wordcloud_editor = models.BooleanField(default=False)
  disable_upgrade = models.BooleanField(default=False)
  email_support = models.BooleanField(default=False)

  question_types = models.ManyToManyField(TierQuestionType)
  end_actions = models.ManyToManyField(TierEndAction)

  # def question_types_default (self):
  #   return self.question_type_choices

  # def end_actions_default (self):
  #   return self.end_actions_default

  def __str__(self):
    return str(self.name)

class CustomTier(Tier):
  pass

class RegularTier(Tier):
  pass
