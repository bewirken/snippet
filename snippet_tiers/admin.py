from django.contrib import admin

from .models import RegularTier, CustomTier, TierQuestionType, TierEndAction


class RegularTierAdmin(admin.ModelAdmin):
  model = RegularTier
  list_display = ['id', 'name', 'polls_amount',
                  'questions_amount',
                  'participants_amount',
                  'user_answers_amount',
                  'image_uploads_amount',
                  'conditional_analysis',
                  'wordcloud_editor',
                  'disable_upgrade',
                  'email_support',
                  ]

  filter_horizontal = ('question_types', 'end_actions',)


class CustomTierAdmin(admin.ModelAdmin):
  model = CustomTier
  list_display = ['id', 'name', 'polls_amount',
                  'questions_amount',
                  'participants_amount',
                  'user_answers_amount',
                  'image_uploads_amount',
                  'conditional_analysis',
                  'wordcloud_editor',
                  'disable_upgrade',
                  'email_support',
                  ]
  filter_horizontal = ('question_types', 'end_actions',)


class TierQuestionTypesAdmin(admin.ModelAdmin):
  model = TierQuestionType
  list_display = ['name']


class TierEndActionsAdmin(admin.ModelAdmin):
  model = TierEndAction
  list_display = ['name']


admin.site.register(RegularTier, RegularTierAdmin)
admin.site.register(CustomTier, CustomTierAdmin)
admin.site.register(TierQuestionType, TierQuestionTypesAdmin)
admin.site.register(TierEndAction, TierEndActionsAdmin)
