pytz==2019.3  # https://github.com/stub42/pytz
python-slugify==4.0.0  # https://github.com/un33k/python-slugify
Pillow==7.1.2  # https://github.com/python-pillow/Pillow
argon2-cffi==19.2.0  # https://github.com/hynek/argon2_cffi
whitenoise==5.0.1  # https://github.com/evansd/whitenoise
redis==3.4.1 # https://github.com/andymccurdy/redis-py
uvicorn==0.11.3  # https://github.com/encode/uvicorn
gunicorn==20.0.4  # https://github.com/benoitc/gunicorn


# Django-Cookiecutter
# ------------------------------------------------------------------------------
django==3.0.5  # pyup: < 3.1  # https://www.djangoproject.com/
django-environ==0.4.5  # https://github.com/joke2k/django-environ
django-model-utils==4.0.0  # https://github.com/jazzband/django-model-utils
# django-allauth==0.41.0  # https://github.com/pennersr/django-allauth
# django-crispy-forms==1.9.0  # https://github.com/django-crispy-forms/django-crispy-forms
django-redis==4.11.0  # https://github.com/jazzband/django-redis


# MANUALLY INSTALLED

# PostgresSQL Adapter
# ------------------------------------------------------------------------------
psycopg2==2.8.5 --no-binary psycopg2  # https://github.com/psycopg/psycopg2

#Webpack Loader
# ------------------------------------------------------------------------------
django-webpack-loader==0.7.0 # https://github.com/owais/django-webpack-loader

#Digital Ocean Spaces (AWS3)
# ------------------------------------------------------------------------------
django-storages[boto3]==1.9.1  # https://github.com/jschneier/django-storages

# Django GraphQL
# ------------------------------------------------------------------------------
graphene-django==2.15.0 # https://github.com/graphql-python/graphene-django
graphene-file-upload==1.3.0 # https://github.com/lmcgartland/graphene-file-upload
django-graphql-jwt==0.3.4 # https://github.com/flavors/django-graphql-jwt
django-cors-headers==3.2.1 # https://github.com/adamchainz/django-cors-headers

# Django Anymail
# ------------------------------------------------------------------------------
django-anymail[sendinblue]==8.6  # https://github.com/anymail/django-anymail

# Django Import-Export
# ------------------------------------------------------------------------------
django-import-export==2.2.0 # https://github.com/django-import-export/django-import-export

# Django RichTextEditor (CKEditor)
# ------------------------------------------------------------------------------
django-ckeditor==5.9.0 # https://github.com/django-ckeditor/django-ckeditor

# Natural Language Processing (NLP)
# ------------------------------------------------------------------------------
nltk==3.5 # https://github.com/nltk/nltk

# Django Requests
# ------------------------------------------------------------------------------
requests==2.26.0 # https://github.com/psf/requests

# CRON Jobs
# ------------------------------------------------------------------------------
# django-crontab==0.7.1