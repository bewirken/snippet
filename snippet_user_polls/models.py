from django.db import models
from django.conf import settings
from django.apps import apps as django_apps


class UserPoll(models.Model):
  poll = models.ForeignKey('snippet_polls.Poll', on_delete=models.CASCADE, null=True)
  user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
  success_email = models.EmailField(blank=True, null=True, default='')

  class Meta:
    unique_together = ['user', 'poll']

  def __str__(self):
    return str(self.poll)

  def save(self, *args, **kwargs):

    # If UserPoll does already exist save it
    if UserPoll.objects.filter(user=self.user, poll=self.poll).exists():
      super(UserPoll, self).save(*args, **kwargs)

    # Else create a new UserPoll
    else:
      super(UserPoll, self).save(*args, **kwargs)

      # Create new UserAnswers for every Question in Poll
      models_dict = {
          'QuestionOpen': 'UserAnswerOpen',
          'QuestionYesOrNo': 'UserAnswerYesOrNo',
          'QuestionMultiple': 'UserAnswerMultiple',
          'QuestionSingle': 'UserAnswerSingle',
          'QuestionRange': 'UserAnswerRange',
          'QuestionExplainer': 'UserAnswerExplainer',
      }

      for key_model, value_model in models_dict.items():

        UserAnswerModel = django_apps.get_model('snippet_user_answers', value_model)
        QuestionModel = django_apps.get_model('snippet_questions', key_model)

        questions_by_poll = QuestionModel.objects.filter(poll=self.poll)

        user_answers_list = []

        for question in questions_by_poll:
          user_answers_list.append(UserAnswerModel(user=self.user, poll=self.poll, question=question))
        UserAnswerModel.objects.bulk_create(user_answers_list)
