from django.apps import AppConfig


class SnippetUserPollsConfig(AppConfig):
    name = 'snippet_user_polls'
