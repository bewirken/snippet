from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import views as auth_views

from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, path, re_path
from django.views import defaults as default_views
from django.views.generic import TemplateView
from django.views.decorators.csrf import csrf_exempt


# from graphene_django.views import GraphQLView
from graphene_file_upload.django import FileUploadGraphQLView


urlpatterns = [
    # Password Reset complete
    path('accounts/reset/done/', TemplateView.as_view(template_name="registration/password_reset_complete.html")),
    
    # Password Reset Mail: Explicitly define html_email_template_name to avoid loss of styling
    path('accounts/password_reset/', auth_views.PasswordResetView.as_view(
        html_email_template_name='registration/password_reset_email.html'
    )),
    
    # Accounts URLS for other password reset Templates 
    path('accounts/', include('django.contrib.auth.urls')),

    #Vue Admin
    path("admin/", TemplateView.as_view(template_name="vue-admin.html"), name="admin"),

    # GraphQL Enpoint
    path(settings.GRAPHQL_URL, csrf_exempt(FileUploadGraphQLView.as_view(graphiql=settings.GRAPHIQL))),
    
    #Django Admin
    path(settings.ADMIN_URL, admin.site.urls),

    # Every other path leads to Vue Frontend
    re_path(r'^(?P<path>.*)/$', TemplateView.as_view(template_name="vue-frontend.html")),
    path("", TemplateView.as_view(template_name="vue-frontend.html")),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
if settings.DEBUG:
    # Static file serving when using Gunicorn + Uvicorn for local web socket development
    urlpatterns += staticfiles_urlpatterns()


if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        path(
            "400/",
            default_views.bad_request,
            kwargs={"exception": Exception("Bad Request!")},
        ),
        path(
            "403/",
            default_views.permission_denied,
            kwargs={"exception": Exception("Permission Denied")},
        ),
        path(
            "404/",
            default_views.page_not_found,
            kwargs={"exception": Exception("Page not Found")},
        ),
        path("500/", default_views.server_error),
    ]
    if "debug_toolbar" in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns = [path("__debug__/", include(debug_toolbar.urls))] + urlpatterns
