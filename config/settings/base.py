"""
Base settings to build other settings files upon.
"""
import os
from pathlib import Path

import environ

ROOT_DIR = Path(__file__).parents[2]
# snippet/)
APPS_DIR = ROOT_DIR / "snippet"
env = environ.Env()

READ_DOT_ENV_FILE = env.bool("DJANGO_READ_DOT_ENV_FILE", default=False)
if READ_DOT_ENV_FILE:
  # OS environment variables take precedence over variables from .env
  env.read_env(str(ROOT_DIR / ".env"))

# GENERAL
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#debug
DEBUG = env.bool("DJANGO_DEBUG", False)
# Local time zone. Choices are
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# though not all of them may be available with every OS.
# In Windows, this must be set to your system time zone.
TIME_ZONE = "Europe/Berlin"
# https://docs.djangoproject.com/en/dev/ref/settings/#language-code
LANGUAGE_CODE = "de-de"
# https://docs.djangoproject.com/en/dev/ref/settings/#site-id
SITE_ID = 1
# https://docs.djangoproject.com/en/dev/ref/settings/#use-i18n
USE_I18N = True
# https://docs.djangoproject.com/en/dev/ref/settings/#use-l10n
USE_L10N = True
# https://docs.djangoproject.com/en/dev/ref/settings/#use-tz
USE_TZ = True
# https://docs.djangoproject.com/en/dev/ref/settings/#locale-paths
LOCALE_PATHS = [str(ROOT_DIR / "locale")]

# DATABASES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#databases
DATABASES = {"default": env.db("DATABASE_URL")}
DATABASES["default"]["ATOMIC_REQUESTS"] = True
# URLS
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#root-urlconf
ROOT_URLCONF = "config.urls"
# https://docs.djangoproject.com/en/dev/ref/settings/#wsgi-application
WSGI_APPLICATION = "config.wsgi.application"

# APPS
# ------------------------------------------------------------------------------
DJANGO_APPS = [
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.sites",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.admin",
    "django.forms",
]
THIRD_PARTY_APPS = [
    # "allauth",
    # "allauth.account",
    # "allauth.socialaccount",
    "graphene_django",
    "corsheaders",
    "webpack_loader",
    "import_export",
    "ckeditor",
    # "django_crontab",
]

LOCAL_APPS = [
    "snippet.users.apps.UsersConfig",
    # Your stuff: custom apps go here
    "snippet_accounts.apps.SnippetAccountsConfig",
    "snippet_notifications.apps.SnippetNotificationsConfig",
    "snippet_images.apps.SnippetImagesConfig",
    "snippet_impress_privacy.apps.SnippetImpressPrivacyConfig",
    "snippet_polls.apps.SnippetPollsConfig",
    "snippet_questions.apps.SnippetQuestionsConfig",
    "snippet_tans.apps.SnippetTansConfig",
    "snippet_tiers.apps.SnippetTiersConfig",
    "snippet_user_answers.apps.SnippetUserAnswersConfig",
    "snippet_user_polls.apps.SnippetUserPollsConfig",
]
# https://docs.djangoproject.com/en/dev/ref/settings/#installed-apps
INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

# MIGRATIONS
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#migration-modules
MIGRATION_MODULES = {"sites": "snippet.contrib.sites.migrations"}

# AUTHENTICATION
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#authentication-backends
AUTHENTICATION_BACKENDS = [
    "graphql_jwt.backends.JSONWebTokenBackend",
    "django.contrib.auth.backends.ModelBackend",
    # "allauth.account.auth_backends.AuthenticationBackend",
]
# https://docs.djangoproject.com/en/dev/ref/settings/#auth-user-model
AUTH_USER_MODEL = "users.User"
# https://docs.djangoproject.com/en/dev/ref/settings/#login-redirect-url
LOGIN_REDIRECT_URL = "users:redirect"
# https://docs.djangoproject.com/en/dev/ref/settings/#login-url
LOGIN_URL = "account_login"

# PASSWORDS
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#password-hashers
PASSWORD_HASHERS = [
    # https://docs.djangoproject.com/en/dev/topics/auth/passwords/#using-argon2-with-django
    "django.contrib.auth.hashers.Argon2PasswordHasher",
    "django.contrib.auth.hashers.PBKDF2PasswordHasher",
    "django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher",
    "django.contrib.auth.hashers.BCryptSHA256PasswordHasher",
]
# https://docs.djangoproject.com/en/dev/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator"
    },
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator"},
    {"NAME": "django.contrib.auth.password_validation.CommonPasswordValidator"},
    {"NAME": "django.contrib.auth.password_validation.NumericPasswordValidator"},
]

# MIDDLEWARE
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#middleware
MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.common.BrokenLinkEmailsMiddleware",
    # "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

# STORAGES
# ------------------------------------------------------------------------------
# https://django-storages.readthedocs.io/en/latest/#installation
INSTALLED_APPS += ["storages"]  # noqa F405
# https://django-storages.readthedocs.io/en/latest/backends/amazon-S3.html#settings
AWS_ACCESS_KEY_ID = env("DJANGO_AWS_ACCESS_KEY_ID")
# https://django-storages.readthedocs.io/en/latest/backends/amazon-S3.html#settings
AWS_SECRET_ACCESS_KEY = env("DJANGO_AWS_SECRET_ACCESS_KEY")
# https://django-storages.readthedocs.io/en/latest/backends/amazon-S3.html#settings
AWS_STORAGE_BUCKET_NAME = env("DJANGO_AWS_STORAGE_BUCKET_NAME")
# https://django-storages.readthedocs.io/en/latest/backends/amazon-S3.html#settings

AWS_S3_ENDPOINT_URL = "https://fra1.digitaloceanspaces.com"
AWS_S3_ENDPOINT_ID = env("DJANGO_AWS_ENPOINT_ID")

DIGITAL_OCEAN_API_TOKEN = env("DJANGO_DIGITAL_OCEAN_API_TOKEN")



AWS_DEFAULT_ACL = "public-read"
AWS_QUERYSTRING_AUTH = False

# DO NOT change these unless you know what you"re doing.
_AWS_EXPIRY = 60 * 60 * 24 * 7
# https://django-storages.readthedocs.io/en/latest/backends/amazon-S3.html#settings
AWS_S3_OBJECT_PARAMETERS = {
    "CacheControl": f"max-age={_AWS_EXPIRY}, s-maxage={_AWS_EXPIRY}, must-revalidate",
}


# STATIC
# ------------------------------------------------------------------------------
STATICFILES_LOCATION = env("DJANGO_AWS_STATS_FOLDER_NAME") # only used for snippet/custom_storages.py
# STATICFILES_STORAGE is defined per sub-setting (local, staging, production)

# https://docs.djangoproject.com/en/dev/ref/settings/#static-root
STATIC_ROOT = str(ROOT_DIR / "staticfiles")
# https://docs.djangoproject.com/en/dev/ref/settings/#static-url
STATIC_URL = "/static/"
# https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#staticfiles-finders
STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
]

STATICFILES_DIRS = [str(APPS_DIR / "static")]

# http://whitenoise.evans.io/en/latest/django.html#using-whitenoise-in-development
#INSTALLED_APPS = ["whitenoise.runserver_nostatic"] + INSTALLED_APPS  # noqa F405
#MIDDLEWARE += ["whitenoise.middleware.WhiteNoiseMiddleware"]  # noqa F405
# https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS


# MEDIA
# ------------------------------------------------------------------------------
MEDIAFILES_LOCATION = "media"
DEFAULT_FILE_STORAGE = "snippet.custom_storages.MediaStorage"

# https://docs.djangoproject.com/en/dev/ref/settings/#media-root
MEDIA_ROOT = str(APPS_DIR / "media")
# https://docs.djangoproject.com/en/dev/ref/settings/#media-url
MEDIA_URL = "/media/"


# TEMPLATES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#templates

TEMPLATES = [
    {
        # https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-TEMPLATES-BACKEND
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        # https://docs.djangoproject.com/en/dev/ref/settings/#template-dirs
        "DIRS": [str(APPS_DIR / "templates")],
        "OPTIONS": {
            # https://docs.djangoproject.com/en/dev/ref/settings/#template-loaders
            # https://docs.djangoproject.com/en/dev/ref/templates/api/#loader-types
            "loaders": [
                "django.template.loaders.filesystem.Loader",
                "django.template.loaders.app_directories.Loader",
            ],
            # https://docs.djangoproject.com/en/dev/ref/settings/#template-context-processors
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.template.context_processors.i18n",
                "django.template.context_processors.media",
                "django.template.context_processors.static",
                "django.template.context_processors.tz",
                "django.contrib.messages.context_processors.messages",
                "snippet.utils.context_processors.settings_context",
            ],
            'libraries':{
                'render_inside': 'snippet.templatetags.render_inside',
            }
        },
    }
]

# https://docs.djangoproject.com/en/dev/ref/settings/#form-renderer
FORM_RENDERER = "django.forms.renderers.TemplatesSetting"

# http://django-crispy-forms.readthedocs.io/en/latest/install.html#template-packs
# CRISPY_TEMPLATE_PACK = "bootstrap4"


# FIXTURES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#fixture-dirs
FIXTURE_DIRS = (str(APPS_DIR / "fixtures"),)


# SECURITY
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#session-cookie-httponly
SESSION_COOKIE_HTTPONLY = True
# https://docs.djangoproject.com/en/dev/ref/settings/#csrf-cookie-httponly
CSRF_COOKIE_HTTPONLY = True
# https://docs.djangoproject.com/en/dev/ref/settings/#secure-browser-xss-filter
SECURE_BROWSER_XSS_FILTER = True
# https://docs.djangoproject.com/en/dev/ref/settings/#x-frame-options
# X_FRAME_OPTIONS = "SAMEORIGIN"

# EMAIL
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#default-from-email
DEFAULT_FROM_EMAIL = env(
    "DJANGO_DEFAULT_FROM_EMAIL", default="askit <info@askit-app.de>"
)
# https://docs.djangoproject.com/en/dev/ref/settings/#server-email
SERVER_EMAIL = env("DJANGO_SERVER_EMAIL", default=DEFAULT_FROM_EMAIL)
# https://docs.djangoproject.com/en/dev/ref/settings/#email-subject-prefix
EMAIL_SUBJECT_PREFIX = env(
    "DJANGO_EMAIL_SUBJECT_PREFIX", default="[askit]"
)
# https://docs.djangoproject.com/en/dev/ref/settings/#email-timeout
EMAIL_TIMEOUT = 5
# https://docs.djangoproject.com/en/dev/ref/settings/#email-backend
EMAIL_BACKEND = 'anymail.backends.sendinblue.EmailBackend'

# Anymail (Sendinblue)
# ------------------------------------------------------------------------------
# https://anymail.readthedocs.io/en/stable/installation/#installing-anymail
INSTALLED_APPS += ["anymail"]  # noqa F405
# https://anymail.readthedocs.io/en/stable/installation/#anymail-settings-reference
# https://anymail.dev/en/stable/esps/sendinblue/
ANYMAIL = {
    "SENDINBLUE_API_KEY": env("SENDINBLUE_API_KEY"),
}



# ADMIN
# ------------------------------------------------------------------------------
# Django Admin URL.
ADMIN_URL = "django-admin/"
# https://docs.djangoproject.com/en/dev/ref/settings/#admins
ADMINS = [("""Jan Moll""", "")]
# https://docs.djangoproject.com/en/dev/ref/settings/#managers
MANAGERS = ADMINS

# LOGGING
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#logging
# See https://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "verbose": {
            "format": "%(levelname)s %(asctime)s %(module)s "
            "%(process)d %(thread)d %(message)s"
        }
    },
    "handlers": {
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "verbose",
        }
    },
    "root": {"level": "INFO", "handlers": ["console"]},
}


# django-allauth
# ------------------------------------------------------------------------------
# ACCOUNT_ALLOW_REGISTRATION = env.bool("DJANGO_ACCOUNT_ALLOW_REGISTRATION", True)
# # https://django-allauth.readthedocs.io/en/latest/configuration.html
# ACCOUNT_AUTHENTICATION_METHOD = "username"
# # https://django-allauth.readthedocs.io/en/latest/configuration.html
# ACCOUNT_EMAIL_REQUIRED = True
# # https://django-allauth.readthedocs.io/en/latest/configuration.html
# ACCOUNT_EMAIL_VERIFICATION = "mandatory"
# # https://django-allauth.readthedocs.io/en/latest/configuration.html
# ACCOUNT_ADAPTER = "snippet.users.adapters.AccountAdapter"
# # https://django-allauth.readthedocs.io/en/latest/configuration.html
# SOCIALACCOUNT_ADAPTER = "snippet.users.adapters.SocialAccountAdapter"


# Your stuff...
# ------------------------------------------------------------------------------

# CRON
# CRONJOBS = [
#     ('*/30 * * * * ', 'snippet.cron.preload_user_answers', '>> /app/django-cron.log')
# ]
# CRONTAB_COMMAND_SUFFIX = '2>&1' # write errors to log

# graphene
# ------------------------------------------------------------------------------
GRAPHENE = {
    "SCHEMA": "snippet.graphql_schema.schema",
    "MIDDLEWARE": [
        "graphql_jwt.middleware.JSONWebTokenMiddleware",
    ],
}
GRAPHQL_URL = "graphql"
GRAPHIQL = True

# webpack-loader
# ------------------------------------------------------------------------------

WEBPACK_LOADER = {
    "FRONTEND": {
        "CACHE": not DEBUG,
        "BUNDLE_DIR_NAME": "vue-frontend/",
        #  --- This allows loading webpack-stats via URL instead of locally (see STATS_FILE). But creates latency, which can lead to loading erros.
        # "LOADER_CLASS": "snippet.utils.ext_webpack_loader.ExternalWebpackLoader",
        # "STATS_URL": os.path.join("https://challenge-app-space.fra1.cdn.digitaloceanspaces.com", STATICFILES_LOCATION, "vue-frontend", "webpack-stats-frontend.json"),
        # --- END
        "STATS_FILE": os.path.join(APPS_DIR, "static", "vue-frontend", "webpack-stats-frontend.json")
    },
    "BACKEND": {
        "CACHE": not DEBUG,
        "BUNDLE_DIR_NAME": "vue-admin/",
        #  --- This allows loading webpack-stats via URL instead of locally (see STATS_FILE). But creates latency, which can lead to loading erros.
        # "LOADER_CLASS": "snippet.utils.ext_webpack_loader.ExternalWebpackLoader",
        # "STATS_URL": os.path.join("https://challenge-app-space.fra1.cdn.digitaloceanspaces.com", STATICFILES_LOCATION, "vue-admin", "webpack-stats-admin.json"),
        # --- END
        "STATS_FILE": os.path.join(APPS_DIR, "static", "vue-admin", "webpack-stats-admin.json")
    }
}

#  Password for autogenerated users
# ------------------------------------------------------------------------------
USER_PASSWORD = env("USER_PASSWORD")

# Name for placeholder poll so created superuser has a poll to avoid errors
# ------------------------------------------------------------------------------
PLACEHOLDER_POLL_NAME = ("Platzhalter Umfrage")

# CKEditor 5.9.0 ------------------------------------------------------------------------------
CKEDITOR_BASEPATH = 'https://challenge-app-space.fra1.cdn.digitaloceanspaces.com/snippetfiles/ckeditor/ckeditor/'

CKEDITOR_CONFIGS = {
    'default': {
        'height': 100,
        'width': 420,
        'autoParagraph': False,
        'toolbar': 'Custom',
        'toolbar_Custom': [
            ['Format', 'Bold', 'Italic'],
            ['Image', 'Link', 'Unlink'],
            ['Source']
        ]
    },
    'extended': {
        'height': 100,
        'width': 420,
        'autoParagraph': True,
        'toolbar': 'Custom',
        'toolbar_Custom': [
            ['Format', 'Bold', 'Italic', 'NumberedList', 'BulletedList'],
            ['Image', 'Link', 'Unlink'],
            ['Source']
        ]
        # 'toolbar_Custom': [
        #     ['TextColor','Format', 'Bold', 'Italic', 'Subscript', 'Superscript','NumberedList', 'BulletedList'],
        #     ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
        #     ['Image','Link','Unlink'],
        #     ['HorizontalRule', 'Source']
        # ],
        # 'toolbar_YourCustomToolbarConfig': [
        #     {'name': 'document', 'items': ['Source']},
        #     {'name': 'clipboard', 'items': ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
        #     {'name': 'editing', 'items': ['Find', 'Replace', '-', 'SelectAll']},
        #     {'name': 'forms',
        #      'items': ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton',
        #                'HiddenField']},
        #     '/',
        #     {'name': 'basicstyles',
        #      'items': ['Bold', 'Italic', 'Underline', 'Strike',  '-', 'RemoveFormat']},
        #     {'name': 'paragraph',
        #      'items': ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-',
        #                'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl',
        #                'Language']},
        #     {'name': 'links', 'items': ['Link', 'Unlink', 'Anchor']},
        #     {'name': 'insert',
        #      'items': ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe']},
        #     '/',
        #     {'name': 'styles', 'items': ['Styles', 'Format', 'Font', 'FontSize']},
        #     {'name': 'colors', 'items': [, 'BGColor']},
        #     {'name': 'tools', 'items': ['Maximize', 'ShowBlocks']},
        #     {'name': 'about', 'items': ['About']},
        #     '/',  # put this to force next toolbar on new line
        #     {'name': 'yourcustomtools', 'items': [
        #         # put the name of your editor.ui.addButton here
        #         'Preview',
        #         'Maximize',

        #     ]},
        # ],
    },

}
