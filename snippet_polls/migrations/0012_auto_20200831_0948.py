# Generated by Django 3.0.5 on 2020-08-31 07:48

import ckeditor.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('snippet_polls', '0011_poll_poll_verification_mail'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='poll',
            name='poll_verification_mail',
        ),
        migrations.AddField(
            model_name='poll',
            name='poll_email_header',
            field=ckeditor.fields.RichTextField(blank=True, null=True, verbose_name='Bestätigungsmail Überschriften'),
        ),
        migrations.AddField(
            model_name='poll',
            name='poll_participation_terms',
            field=ckeditor.fields.RichTextField(blank=True, null=True, verbose_name='Teilnahmebedingungen'),
        ),
    ]
