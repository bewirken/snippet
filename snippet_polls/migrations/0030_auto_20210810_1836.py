# Generated by Django 3.0.5 on 2021-08-10 16:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('snippet_polls', '0029_auto_20210805_2053'),
    ]

    operations = [
        migrations.AddField(
            model_name='poll',
            name='poll_participants_counter',
            field=models.IntegerField(blank=True, default=0, null=True),
        ),
        migrations.AddField(
            model_name='poll',
            name='poll_success_emails_counter',
            field=models.IntegerField(blank=True, default=0, null=True),
        ),
    ]
