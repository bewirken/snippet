# Generated by Django 3.0.5 on 2022-11-22 12:41

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('snippet_polls', '0044_poll_poll_email_usage'),
    ]

    operations = [
        migrations.RenameField(
            model_name='poll',
            old_name='poll_success_button_url',
            new_name='poll_redirect_url',
        ),
    ]
