# Generated by Django 3.0.5 on 2022-04-12 10:59

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('snippet_polls', '0036_poll_poll_conditional_question_user_ids'),
    ]

    operations = [
        migrations.RenameField(
            model_name='poll',
            old_name='poll_conditional_question_choice_values',
            new_name='poll_conditional_question_selected_values',
        ),
    ]
