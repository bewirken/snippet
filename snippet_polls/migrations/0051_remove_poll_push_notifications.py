# Generated by Django 3.0.5 on 2023-05-18 18:16

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('snippet_polls', '0050_auto_20230417_1446'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='poll',
            name='push_notifications',
        ),
    ]
