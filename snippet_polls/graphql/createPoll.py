
import graphene
from graphql_jwt.decorators import login_required
from django.core.mail import send_mail
from django.utils.html import strip_tags
from django.template.loader import render_to_string
from django.contrib.sites.models import Site

from snippet_notifications.models import NotificationEmail

# Types
from .__types import PollType

# Models
from ..models import Poll
from snippet_accounts.models import Account


class CreatePollMutation(graphene.Mutation):
  poll = graphene.Field(PollType)

  class Arguments:
    poll_name = graphene.String(required=True)
    poll_description = graphene.String()

  @login_required
  def mutate(self, info, poll_name, **kwargs):

    poll_amount_of_user = Poll.objects.filter(poll_owner=info.context.user).count()
    max_poll_amount_of_tier = Account.objects.select_related("tier").get(user=info.context.user).tier.polls_amount
    poll_description = kwargs.get('poll_description', None)

    if poll_amount_of_user >= max_poll_amount_of_tier:
      raise Exception('Reached max number of polls')

    poll = Poll(
        poll_name=poll_name,
        poll_description=poll_description,
        poll_owner=info.context.user,
    )
    poll.save()

    email_notification = NotificationEmail.objects.get(slug="create-poll")

    subject = email_notification.subject
    html_message = render_to_string('emails/email_main.html', {
        'domain': Site.objects.get_current().domain,
        'user_name': info.context.user.username,
        'poll_name': poll.poll_name,
        'poll_code': poll.poll_code,
        'subject': email_notification.subject,
        'content': email_notification.content,
    })

    plain_message = strip_tags(html_message)
    from_email = 'info@askit-app.de'
    to_email = info.context.user.email
    send_mail(subject, plain_message, from_email, [to_email], html_message=html_message)

    return CreatePollMutation(poll=poll)


class CreatePoll(graphene.ObjectType):
  create_poll = CreatePollMutation.Field()
