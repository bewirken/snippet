from django.apps import apps as django_apps
from django.forms.models import model_to_dict
from snippet_accounts.models import Account

import graphene
from graphql_jwt.decorators import login_required

from .__types import PollType
from ..models import Poll


def duplicate_question(question, poll):
  attributes = {
      "poll": poll,
      "question_type": question.question_type,
      "question_text": question.question_text,
      "question_imagelink": question.question_imagelink,
  }

  if question.question_type in ["Single", "Multiple"]:
    attributes["options"] = question.options

  if question.question_type == "Range":
    attributes["scale"] = question.scale
    attributes["scale_type"] = question.scale_type

  new_question = question.__class__(**attributes)
  new_question.save()


class DuplicatePollMutation(graphene.Mutation):
  poll = graphene.Field(PollType)

  class Arguments:
    poll_id = graphene.ID(required=True)

  @login_required
  def mutate(self, info, poll_id):
    poll = Poll.objects.get(id=poll_id)

    poll_amount_of_user = Poll.objects.filter(poll_owner=info.context.user).count()
    max_poll_amount_of_tier = Account.objects.select_related("tier").get(user=info.context.user).tier.polls_amount

    if poll_amount_of_user >= max_poll_amount_of_tier:
      raise Exception('Reached max number of polls')

    Question = django_apps.get_model('snippet_questions', 'Question')
    QuestionYesOrNo = django_apps.get_model('snippet_questions', 'QuestionYesOrNo')
    QuestionSingle = django_apps.get_model('snippet_questions', 'QuestionSingle')
    QuestionMultiple = django_apps.get_model('snippet_questions', 'QuestionMultiple')
    QuestionOpen = django_apps.get_model('snippet_questions', 'QuestionOpen')
    QuestionRange = django_apps.get_model('snippet_questions', 'QuestionRange')

    new_poll = Poll(
        poll_name=poll.poll_name + " Kopie",
        poll_description=poll.poll_description,
        poll_owner=info.context.user,
    )
    new_poll.save()

    for questionId in poll.ordered_question_ids:
      question = Question.objects.get(id=questionId)

      if question.question_type == "YesOrNo":
        question = QuestionYesOrNo.objects.get(id=questionId)
      elif question.question_type == "Single":
        question = QuestionSingle.objects.get(id=questionId)
      elif question.question_type == "Multiple":
        question = QuestionMultiple.objects.get(id=questionId)
      elif question.question_type == "Open":
        question = QuestionOpen.objects.get(id=questionId)
      elif question.question_type == "Range":
        question = QuestionRange.objects.get(id=questionId)

      duplicate_question(question, new_poll)

    return DuplicatePollMutation(poll=new_poll)


class DuplicatePoll(graphene.ObjectType):
  duplicate_poll = DuplicatePollMutation.Field()
