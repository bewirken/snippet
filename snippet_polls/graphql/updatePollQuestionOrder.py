import graphene
from graphql_jwt.decorators import login_required


# Types
# Models
from ..models import Poll


class UpdatePollQuestionOrderMutation(graphene.Mutation):
  ordered_question_ids = graphene.String()

  class Arguments:
    poll_id = graphene.ID(required=True)
    ordered_question_ids = graphene.List(graphene.Int, required=True)

  @login_required
  def mutate(self, info, poll_id, ordered_question_ids):

    poll = Poll.objects.get(id=poll_id)

    # #1 Check if length of arrays is the NOT same, IF return unchanged question-order array
    if len(poll.ordered_question_ids) != len(ordered_question_ids):
      return UpdatePollQuestionOrderMutation(ordered_question_ids=poll.ordered_question_ids)
    # #2 Check if all previous question-ids are in the array, IF NOT return unchanged question-order array
    for item in poll.ordered_question_ids:
      if item not in ordered_question_ids:
        return UpdatePollQuestionOrderMutation(ordered_question_ids=poll.ordered_question_ids)

    # If Check #1 and #2 dont return
    poll.ordered_question_ids = ordered_question_ids
    poll.save()

    return UpdatePollQuestionOrderMutation(ordered_question_ids=ordered_question_ids)


class UpdatePollQuestionOrder(graphene.ObjectType):
  update_poll_question_order = UpdatePollQuestionOrderMutation.Field()
