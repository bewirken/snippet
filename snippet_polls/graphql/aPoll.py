import graphene
from graphql_jwt.decorators import login_required

# Types
from .__types import PollType

# Models
from ..models import Poll


class APoll(graphene.ObjectType):

  a_poll = graphene.Field(PollType, poll_code=graphene.String(required=True))

  # @login_required
  def resolve_a_poll(self, info, poll_code, **kwarg):
    match_poll = Poll.objects.get(poll_code=poll_code)

    return match_poll
