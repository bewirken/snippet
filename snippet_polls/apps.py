from django.apps import AppConfig


class SnippetPollsConfig(AppConfig):
  name = 'snippet_polls'
