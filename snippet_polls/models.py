from django.db import models
from django.conf import settings
from django.contrib.postgres.fields import ArrayField
from django.utils.crypto import get_random_string

from ckeditor.fields import RichTextField

from snippet.utils.check_strip_attrs import check_strip_attrs


class Poll(models.Model):

  class Status(models.TextChoices):
    ACTIVE = 'active', "Aktiv"
    TEST = 'test', "Test-Modus"
    INACTIVE = 'inactive', "Inaktiv"

  class Language(models.TextChoices):
    DE = 'de', "Deutsch"
    EN = 'en', "Englisch"

  class SuccessButton(models.TextChoices):
    BACK = 'back', "Keine Aktion"
    SEND_MAIL = 'send_mail', "eMail Adresse abschicken"
    REDIRECT = 'redirect', "Weiterleitung"
    TAN = 'tan', "TAN benutzen / verbrauchen"


  class Meta:
    indexes = [
        models.Index(fields=['poll_code', ]),
    ]

  poll_owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, blank=True )
  poll_status = models.CharField(("Umfrage-Status"), max_length=10, choices=Status.choices, default=Status.INACTIVE)
  poll_is_multipoll = models.BooleanField(("Umfrage ist Teil einer Multipoll"), default=False)
  poll_requires_registration = models.BooleanField(("Registrierung per E-Mail nötig"), default=False)
  poll_requires_tan = models.BooleanField(("TAN Eingabe nötig"), default=False)
  poll_name = models.CharField(("Umfrage-Titel"), max_length=140)
  poll_description = models.TextField(("Umfragebeschreibung"), max_length=255, null=True, blank=True)
  poll_language = models.CharField(("Sprache"), max_length=10, choices=Language.choices, default=Language.DE)
  poll_color_primary = models.CharField(("Primäre Farbe"), max_length=48, default="#4A90E2")
  poll_color_secundary = models.CharField(("Sekundäre Farbe"), max_length=48, default="#7ED321")
  poll_code = models.CharField(("Poll Code"), max_length=5, blank=True, unique=True)
  poll_impress = RichTextField(("Impressum"), blank=True, default="")
  poll_support = RichTextField(("Support"), blank=True, default="hilfe@askit-app.de")
  poll_privacy = RichTextField(("Datenschutz"), blank=True, default="")
  poll_startscreen = RichTextField(("Startseite"), blank=True, default="")
  poll_missing_modal = RichTextField(("Abschlussnachricht - Nicht alle Fragen beantwortet"), blank=True, default="")
  poll_success_modal = RichTextField(("Abschlussnachricht - Alle Fragen beantwortet"), blank=True, default="")
  poll_success_button = models.CharField(("Abschlussnachricht - Button"), max_length=10, choices=SuccessButton.choices, default=SuccessButton.BACK)
  poll_redirect_url = models.URLField(("Weiterleitung-URL"), max_length=500, blank=True, default="",)
  poll_redirect_btn_text = models.CharField(("Text für Weiterleitungs Button"), max_length=500, blank=True, default="Weitere Infos")
  poll_email_usage = models.CharField(("Email Verwendungszweck"), max_length=500, blank=True, default="")
  poll_email_header = RichTextField(("Bestätigungsmail Überschriften"), blank=True, default="")
  poll_participation_terms = RichTextField(("Teilnahmebedingungen"), blank=True, default="", config_name='extended')
  ordered_question_ids = ArrayField(models.IntegerField(blank=True), default=list, null=True, blank=True)
  parent_poll = models.ForeignKey('self', on_delete=models.PROTECT, null=True, blank=True)
  poll_participants_counter = models.IntegerField(null=True, blank=True, default=0)
  poll_success_emails_counter = models.IntegerField(null=True, blank=True, default=0)
  poll_filter_question_id = models.IntegerField(null=True, blank=True)
  poll_filter_question_type = models.CharField(max_length=10, null=True, blank=True)
  poll_filter_question_selected_values = ArrayField(models.IntegerField(blank=True), default=list, null=True, blank=True)
  poll_filter_question_user_ids = ArrayField(models.IntegerField(blank=True), default=list, null=True, blank=True)
  created_at = models.DateTimeField(auto_now_add=True)
  updated_at = models.DateTimeField(auto_now=True)


  def __str__(self):
    return self.poll_name

  def save(self, *args, **kwargs):
    if self.poll_requires_tan:
      self.poll_success_button = Poll.SuccessButton.TAN
      
    if self.pk is None:

      self.poll_code = self.generate_poll_code()

      check_strip_attrs(self)
      super(Poll, self).save(*args, **kwargs)

    else:
      check_strip_attrs(self)
      super(Poll, self).save(*args, **kwargs)

  def delete(self, *args, **kwargs):
    if self.poll_name == settings.PLACEHOLDER_POLL_NAME:
      pass

    else:
      super(Poll, self).delete(*args, **kwargs)

  def generate_poll_code(self):
    # Create Poll with unique poll code
    code = get_random_string(length=5).lower()

    # Check if generated_code already exist in the DB and regenerate if true
    while Poll.objects.filter(poll_code=code):
      code = get_random_string(length=5).lower()

    return code
  


