docker compose -f local.yml build
docker compose -f local.yml run --rm django python manage.py migrate
docker compose -f local.yml run --rm django python manage.py createsuperuser

docker compose -f local.yml run --rm django python manage.py collectstatic --noinput
(docker compose -f local.yml up -d --force-recreate --no-deps --build django)

docker compose -f local.yml up
docker compose -f local.yml up -d 
docker compose -f local.yml up -d --build --remove-orphans

docker compose -f local.yml restart

docker compose -f local.yml run --rm django python manage.py makemigrations
docker compose -f local.yml run --rm django python manage.py migrate
docker compose -f local.yml run --rm django python manage.py shell

# Open shell inside docker container
docker compose -f local.yml run django sh

# DANGEROUS - Removes all docker container/images/volumes
# docker rm -f $(docker ps -a -q)
# docker volume rm $(docker volume ls -q)

# When DISC SPACE is full 
docker image ls -f 'dangling=true'
# docker image rm $(docker image ls -f 'dangling=true' -q)

docker compose -f local.yml run --rm django python manage.py startapp <app-title>

# NLTK for Dockerfile
----------------------------------------
RUN [ "python", "-c", "import nltk; nltk.download('stopwords'); nltk.download('punkt')" ]


# Kill running app on port
----------------------------------------
#When http://localhost:9528/ does not work and port is 9529 instead
npx kill-port 9528


# Gitlab Runner Setup
----------------------------------------
curl -LJO "https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_$amd64.deb"
dpkg -i gitlab-runner_amd64.deb

curl -L https://github.com/docker/compose/releases/download/1.27.4/docker compose-`uname -s`-`uname -m` -o /usr/local/bin/docker compose
sudo chmod +x /usr/local/bin/docker compose

base=https://github.com/docker/machine/releases/download/v0.16.0 \
  && curl -L $base/docker-machine-$(uname -s)-$(uname -m) >/tmp/docker-machine \
  && sudo mv /tmp/docker-machine /usr/local/bin/docker-machine \
  && chmod +x /usr/local/bin/docker-machine

curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt install nodejs

sudo npm install -g lerna


# Undo last local GIT commit (not remote yet)
----------------------------------------
git reset --soft HEAD~


# Local Chrome CORS Error:
----------------------------------------
chrome://flags
disable Block insecure private network requests


# EXPORT & IMPORT DATABASE
----------------------------------------
sudo apt-get update
sudo apt-get install postgresql-client

docker exec -t your-db-container pg_dumpall -c -U your-db-user | gzip > ./dump_$(date +"%Y-%m-%d_%H_%M_%S").gz
gunzip < your_dump.sql.gz | docker exec -i your-db-container psql -U your-db-user -d your-db-name
----------------------------------------


# LOCAL DOCKER-MACHINE
----------------------------------------
docker-machine create --driver=generic --generic-ip-address=x.x.x.x --generic-ssh-key ~/.ssh/id_rsa poll-staging


# Reverse Migration - Delete every migration after <migration-nr> then use makemigrations again.
docker compose -f local.yml run --rm django python manage.py migrate <my-app> <migration-nr> # e.g. 0025

# Delete by Python shell
from django.apps import apps as django_apps
from django.contrib.auth import get_user_model

User = get_user_model()
user = User.objects.get(username="Peter")

Poll = django_apps.get_model('snippet_polls', 'Poll')
Poll.objects.filter(poll_owner=user).delete()

# Generate DB Schema Graph
docker compose -f local.yml run --rm django python manage.py graph_models -a --arrow-shape normal -g -X SocialToken,SocialAccount,SocialApp,Site,EmailConfirmation,Image,EmailAddress,LogEntry,Group,Permission,Session,AbstractBaseSession,ContentType,AbstractBaseUser,,PermissionsMixin -o my_project.png
