from django.template import Library, Template

register = Library()

@register.simple_tag(takes_context=True)
def render_inside(context, string):
    template = Template(string)
    return template.render(context)