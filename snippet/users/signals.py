import re

from anymail.exceptions import AnymailCancelSend
from anymail.signals import pre_send
from django.dispatch import receiver

@receiver(pre_send)
def filter_by_username(sender, message, **kwargs):
    # "pa-" is the prefix for participant users  
    has_participant_name_in_mail = re.search(r'pa-', message.body)
    is_password_reset_mail = re.search(r'zurücksetzen', message.subject)

    if has_participant_name_in_mail and is_password_reset_mail:
        raise AnymailCancelSend("Blocked") 
