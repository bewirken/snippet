import graphene
from graphql_jwt.decorators import login_required

# Types
# Models

class UpdateUserPendingMultipollsMutation(graphene.Mutation):
  success = graphene.Boolean()

  class Arguments:
    pending_multipolls = graphene.List(graphene.List(graphene.Int), required=True)

  @login_required
  def mutate(self, info, pending_multipolls):


    current_user = info.context.user
    current_user.pending_multipolls = pending_multipolls
    current_user.save()
    success = True
    return success


class UpdateUserPendingMultipolls(graphene.ObjectType):
  update_user_pending_multipolls = UpdateUserPendingMultipollsMutation.Field()
