from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags

import re
import graphene
from graphql_jwt.decorators import login_required

# Types
# Models
from snippet_notifications.models import NotificationEmail


class UpdateUserEmailMutation(graphene.Mutation):
  success = graphene.Boolean()

  class Arguments:
    email = graphene.String(required=True)

  @login_required
  def mutate(self, info, email):
    current_user = info.context.user
    poll = info.context.user.current_poll

    success = valitdate_save_send_email(info, current_user, poll, email)
    return UpdateUserEmailMutation(success=success)


class UpdateUserEmail(graphene.ObjectType):
  update_user_email = UpdateUserEmailMutation.Field()


def valitdate_save_send_email(info, user, poll, email):
  # Validate and save email
  if not re.match(r'(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)', email):
    return False
    # raise Exception('Email must be a vaild E-Mail-Adress!')
  else:
    user.email = email
    user.save()


    email_notification = NotificationEmail.objects.get(slug="email-confirmation")

    subject = email_notification.subject
    html_message = render_to_string('emails/email_main.html', {
        'domain': info.context.META.get("HTTP_ORIGIN"),
        'poll_email_header': poll.poll_email_header,
        'poll_impress': poll.poll_impress,
        'poll_participation_terms': poll.poll_participation_terms,
        'poll_privacy': poll.poll_privacy,
        'poll_code' : poll.poll_code,
        'activation_key': user.activation_key,
        'subject': email_notification.subject,
        'content': email_notification.content,
    })
  # ------------------------------------------

  plain_message = strip_tags(html_message)
  from_email = 'info@askit-app.de'
  to = email
  send_mail(subject, plain_message, from_email, [to], html_message=html_message)
  return True
