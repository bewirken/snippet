import hashlib

from django.db import models
from django.urls import reverse
from django.conf import settings
from django.contrib.postgres.fields import ArrayField
from django.apps import apps as django_apps

from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _
from django.utils.crypto import get_random_string


class User(AbstractUser):
  unique_identifier = models.CharField(max_length=100, unique=True, blank=True, null=True)
  name = models.CharField(_("Name of User"), max_length=255, blank=True)
  current_poll = models.ForeignKey('snippet_polls.Poll', on_delete=models.SET_NULL, null=True)
  pending_multipolls = ArrayField(ArrayField(models.IntegerField(blank=True), default=list,
                                  null=True, blank=True), default=list, null=True, blank=True)
  browserInfo = models.CharField(max_length=100, blank=True, null=True)
  osInfo = models.CharField(max_length=100, blank=True, null=True)
  activation_key = models.CharField(max_length=100, blank=True, null=True)

  def get_absolute_url(self):
    return reverse("users:detail", kwargs={"username": self.username})

  def __str__(self):
    return self.username

  def save(self, *args, **kwargs):
    UserPollModel = django_apps.get_model('snippet_user_polls', 'UserPoll')

    # If User doesn't already exists create User and UserPoll
    if self.pk is None:

      # generate and set activation key
      self.activation_key = self.generate_activation_key()

      self.create_placeholder_poll_if_superuser()

      # if multipoll extract _polls and set list
      if self.current_poll:
        if self.current_poll.poll_is_multipoll:
          PollModel = django_apps.get_model('snippet_polls', 'Poll')
          multipolls = PollModel.objects.filter(parent_poll=self.current_poll)

          for poll in multipolls:
            self.pending_multipolls.append([poll.id, 0])

      super(User, self).save(*args, **kwargs)
    # Create UserPoll
      self.create_userPoll(UserPollModel)

    # If a UserPoll for this user / poll combination does not exist create a new UserPoll
    elif not UserPollModel.objects.filter(user=self, poll=self.current_poll).exists():

      self.create_userPoll(UserPollModel)
      super(User, self).save(*args, **kwargs)

    else:
      super(User, self).save(*args, **kwargs)

  def generate_activation_key(self):
    chars = 'abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)'
    secret_key = get_random_string(20, chars)
    return hashlib.sha256((secret_key + self.username).encode('utf-8')).hexdigest()

  def create_userPoll(self, UserPollModel):
    # Very dirty hack to avoid the creation of placeholder UserPolls
    if self.current_poll:
      if self.current_poll.poll_name != settings.PLACEHOLDER_POLL_NAME:
        user_poll = UserPollModel(user=self, poll=self.current_poll)
        user_poll.save()

  def create_placeholder_poll_if_superuser(self):
    # Called on createsuperuser to prevent errors like: 'NoneType' object has no attribute 'poll_name'
    if self.is_superuser:
      PollModel = django_apps.get_model('snippet_polls', 'Poll')
      placeholder_poll = PollModel(
          poll_name=settings.PLACEHOLDER_POLL_NAME,
          poll_description="Platzhalter",
          # poll_creator = currentUser,
      )
      placeholder_poll.save()
      self.current_poll = placeholder_poll

  # def poll_changed(self):
  #   pre_save_poll_id = User.objects.get(pk=self.pk).current_poll_id
  #   print("Poll Changed" + str(pre_save_poll_id) + " " + str(self.current_poll_id))
  #   if pre_save_poll_id == self.current_poll_id:
  #     return False
  #   else:
  #     return True
